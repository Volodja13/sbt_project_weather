//
//  main.m
//  SBT_Project
//
//  Created by Vovan on 22.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
