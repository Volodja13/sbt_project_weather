//
//  TableViewRowAction.h
//  SBT_Project
//
//  Created by Vovan on 27.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewRowAction : UITableViewRowAction

- (void)setImage:(UIImage*)image;

@end
