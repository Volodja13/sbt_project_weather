//
//  CityWeatherCellDC.m
//  SBT_Project
//
//  Created by Vovan on 26.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CityWeatherCellDC.h"
#import "CityWeather.h"

@interface CityWeatherCellDC ()

@property (weak, nonatomic) IBOutlet UILabel *CityLabel;

@property (weak, nonatomic) IBOutlet UILabel *TemperatureLabel;

@end

@implementation CityWeatherCellDC

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCityWeather:(CityWeather*)cityWeather{
    self.CityLabel.text = cityWeather.city;
    self.TemperatureLabel.text =  [NSString stringWithFormat:@"%@ ℃", cityWeather.temp.stringValue];
}

@end
