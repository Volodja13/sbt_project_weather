//
//  CityWeatherCellDC.h
//  SBT_Project
//
//  Created by Vovan on 26.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CityWeather;


@interface CityWeatherCellDC : UITableViewCell

- (void)setCityWeather:(CityWeather*)cityWeather;

@end
