//
//  CityUseCell.h
//  SBT_Project
//
//  Created by Vovan on 24.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@class City;

@interface CityUseCell : UITableViewCell


- (void)setCity:(City*)city;
//- (void)setDelegate:(id)delegate;

@end
