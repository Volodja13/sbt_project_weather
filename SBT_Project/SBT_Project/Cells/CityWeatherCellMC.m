//
//  CityWeatherCellMC.m
//  SBT_Project
//
//  Created by Vovan on 24.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CityWeatherCellMC.h"
#import "CityWeather.h"

@interface CityWeatherCellMC ()

@property (weak, nonatomic) IBOutlet UILabel *City;
@property (weak, nonatomic) IBOutlet UILabel *Temperatyre;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end


@implementation CityWeatherCellMC

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setCityWeather:(CityWeather*)cityWeather{
    self.City.text = cityWeather.city;
    
    if([cityWeather.icon isEqualToString:@"null"])
    {
        self.Temperatyre.text = @"";
        [self.activityIndicator startAnimating];
    }
    else
    {
        self.Temperatyre.text = [NSString stringWithFormat:@"%@ ℃", cityWeather.temp.stringValue];
        [self.activityIndicator stopAnimating];
    }
}

@end
