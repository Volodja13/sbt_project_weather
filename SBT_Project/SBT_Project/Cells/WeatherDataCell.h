//
//  WeatherDataCell.h
//  SBT_Project
//
//  Created by Vovan on 14.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherDataCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;

@end
