//
//  CityUseCell.m
//  SBT_Project
//
//  Created by Vovan on 24.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CityUseCell.h"
#import "City.h"
#import "CitysTableViewController.h"

@interface CityUseCell ()

@property (weak, nonatomic) IBOutlet UILabel *CityLabel;
//@property (nonatomic, weak) id delegate;


@end

@implementation CityUseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

//- (void)setDelegate:(id)delegate{
//    self->_delegate = delegate;
//}

- (void)setCity:(City*)city
{
    self.CityLabel.text = city.city;
}

@end
