//
//  CitysTableViewController.h
//  SBT_Project
//
//  Created by Vovan on 25.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelProtocol.h"

typedef void(^ReturnBlock)();

@interface CitysTableViewController : UITableViewController <UISearchBarDelegate>


- (void)setModel:(id<ModelProtocol>)model;
- (void)setReturnBlock:(ReturnBlock)block;

//- (void)setCityWithID:(NSNumber*)geoID useValue:(BOOL)value;


@end
