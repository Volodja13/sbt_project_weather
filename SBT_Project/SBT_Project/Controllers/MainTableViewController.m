//
//  MainTableViewController.m
//  SBT_Project
//
//  Created by Vovan on 25.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "MainTableViewController.h"
#import "CityWeatherCellMC.h"
#import "CityWeather.h"
#import "ModelProtocol.h"
#import "CitysTableViewController.h"
#import "DetailTableViewController.h"
#import "Model.h"
#import "Assembly.h"
#import "TableViewRowAction.h"

static NSString *kCityWeatherCell = @"CityWeatherCellMCID";
static NSString *kCitySegue = @"CitySegueID";
static NSString *kDetailSegue = @"DetailSegueID";

@interface MainTableViewController ()

@property(nonatomic, strong) id<ModelProtocol> model;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // self.tableView.allowsSelectionDuringEditing = NO;
    
    [Assembly assemblyServiceLocator];
    
    _model = [[Model alloc] init];
    
    [self initController];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)initController{
    [_model loadingWeatherForUsingCityWithSuccessBlock:^{
        [self.tableView reloadData];
    } failedBlock:^(NSString *errorDescription) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model.getWeatherForUsingCitys count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CityWeatherCellMC *cell = [tableView dequeueReusableCellWithIdentifier:kCityWeatherCell forIndexPath:indexPath];
    
    CityWeather *weather = self.model.getWeatherForUsingCitys[indexPath.row];
    [cell setCityWeather:weather];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:kCitySegue])
    {
        [(CitysTableViewController*)[segue destinationViewController] setModel:self.model];
        [(CitysTableViewController*)[segue destinationViewController] setReturnBlock:^{
            [self initController];
        }];
    }
    else if([[segue identifier] isEqualToString:kDetailSegue])
    {
        NSIndexPath *index = [self.tableView indexPathForCell:sender];
        CityWeather *weather = self.model.getWeatherForUsingCitys[index.row];
        [(DetailTableViewController*)[segue destinationViewController] setModel:self.model cityWeather:weather];
    }
    else
    {
        [super prepareForSegue:segue sender:sender];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:kDetailSegue])
    {

        NSIndexPath *index = [self.tableView indexPathForCell:sender];
        CityWeather *weather = self.model.getWeatherForUsingCitys[index.row];
        if([weather.icon isEqualToString:@"null"])
        {
            return NO;
        }
        return YES;
    }
    return YES;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TableViewRowAction *deleteAction = [TableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"       " handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        CityWeather *w = self.model.getWeatherForUsingCitys[indexPath.row];
        [self.model setCityWithID:w.geoId useValue:NO];
        [self.model deleteWeatherWithIndexPath:indexPath];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }];
    
    
    UIImage *image = [UIImage imageNamed:@"tr.png"];
    
    [deleteAction setImage:image];
    
    //deleteAction.backgroundColor = [UIColor colorWithPatternImage:image];
    
    UITableViewRowAction *colorAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Color" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        //
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.backgroundColor = [UIColor blueColor];
        //cell.backgroundView = imageView;
    }];
    colorAction.backgroundColor = [UIColor blueColor];
    
    UITableViewRowAction *alertAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Alert" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        CityWeather *w = self.model.getWeatherForUsingCitys[indexPath.row];
        NSString *alertMessage = [NSString stringWithFormat:@"City %@\nGeoId %@", w.city, w.geoId];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:alertMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];
    alertAction.backgroundColor = [UIColor greenColor];
    return @[deleteAction, colorAction, alertAction];
}

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        CityWeather *w = self.model.getWeatherForUsingCitys[indexPath.row];
        [self.model setCityWithID:w.geoId useValue:NO];
        [self.model deleteWeatherWithIndexPath:indexPath];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        //[self.tableView reloadData];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
