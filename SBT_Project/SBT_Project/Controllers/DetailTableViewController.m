//
//  DetailTableViewController.m
//  SBT_Project
//
//  Created by Vovan on 26.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "DetailTableViewController.h"
#import "ModelProtocol.h"
#import "WeatherDataCell.h"
#import "CityWeather.h"
#import "CityWeatherCellDC.h"

static NSString *kCityWeatherCellDC = @"CityWeatherCellDCID";
static NSString *kWeatherDataCellID = @"WeatherDataCellID";

@interface DetailTableViewController ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) id<ModelProtocol> model;
@property(nonatomic, strong) CityWeather *weather;

@end

@implementation DetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 400;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)setModel:(id<ModelProtocol>)model cityWeather:(CityWeather*)weather{
    self->_model = model;
    self->_weather = weather;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if(indexPath.row == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kCityWeatherCellDC forIndexPath:indexPath];
        [(CityWeatherCellDC*)cell setCityWeather:self.weather];
    }
    else if(indexPath.row == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kWeatherDataCellID forIndexPath:indexPath];
        ((WeatherDataCell*)cell).titleLabel.text = @"Pressure";
        ((WeatherDataCell*)cell).dataLabel.text = [NSString stringWithFormat:@"%@ mom", self.weather.pressure.stringValue];
    }
    else if(indexPath.row == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kWeatherDataCellID forIndexPath:indexPath];
        ((WeatherDataCell*)cell).titleLabel.text = @"Humidity";
        ((WeatherDataCell*)cell).dataLabel.text = [NSString stringWithFormat:@"%@ %%", self.weather.humidity.stringValue];
    }
    else if(indexPath.row == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kWeatherDataCellID forIndexPath:indexPath];
        ((WeatherDataCell*)cell).titleLabel.text = @"Cloudness";
        ((WeatherDataCell*)cell).dataLabel.text =  [NSString stringWithFormat:@"%@ %%", self.weather.cloudness.stringValue];
    }
    else if(indexPath.row == 4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kWeatherDataCellID forIndexPath:indexPath];
        ((WeatherDataCell*)cell).titleLabel.text = @"Wind speed";
        ((WeatherDataCell*)cell).dataLabel.text = [NSString stringWithFormat:@"%@ mps", self.weather.windSpeed.stringValue];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)
    {
        return 180;
    }
    else
    {
        return 80;
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
