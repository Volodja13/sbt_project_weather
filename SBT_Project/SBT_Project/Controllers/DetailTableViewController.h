//
//  DetailTableViewController.h
//  SBT_Project
//
//  Created by Vovan on 26.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelProtocol.h"

@class CityWeather;

@interface DetailTableViewController : UITableViewController

- (void)setModel:(id<ModelProtocol>)model cityWeather:(CityWeather*)weather;

@end
