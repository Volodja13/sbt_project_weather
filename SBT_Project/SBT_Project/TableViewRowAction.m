//
//  TableViewRowAction.m
//  SBT_Project
//
//  Created by Vovan on 27.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "TableViewRowAction.h"

@interface TableViewRowAction()

@property(nonatomic, strong) UIImage *image;

@end

@implementation TableViewRowAction


- (void)setImage:(UIImage*)image{
    self->_image = image;
}

- (void) _setButton:(UIButton*)button
{
    [button setImage:[self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
   // [button imageEdgeInsets].right =
}


@end
