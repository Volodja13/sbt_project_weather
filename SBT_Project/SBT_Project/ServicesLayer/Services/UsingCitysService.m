//
//  UsingCitysService.m
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "UsingCitysService.h"
#import "CityMO+CoreDataClass.h"
#import "AppDelegate.h"
#import "City.h"

@implementation UsingCitysService

- (NSArray*)getSavedCitys{
    NSMutableArray *res = [@[] mutableCopy];
    NSFetchRequest <CityMO*> *fetchRequest = [CityMO fetchRequest];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSArray *citys = [NSArray arrayWithArray:[appDelegate.persistentContainer.viewContext
                     executeFetchRequest:fetchRequest
                     error:nil]];
    for (CityMO *cityMO in citys) {
        City *city = [City new];
        city.geoId = [NSNumber numberWithInt:cityMO.id];
        city.city = cityMO.name;
        [res addObject:city];
    }
    return res;
}


- (void)saveCity:(City*)city{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSEntityDescription *cityDescr = [NSEntityDescription entityForName:@"City" inManagedObjectContext:context];
    
    CityMO *cityMO = [[CityMO alloc] initWithEntity:cityDescr insertIntoManagedObjectContext:context];
    cityMO.id = city.geoId.intValue;
    cityMO.name = city.city;
    
    [appDelegate saveContext];
}

- (void)deleteCity:(City*)city{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest <CityMO*> *fetchRequest = [CityMO fetchRequest];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"id == %@", city.geoId]];
    
    NSError *error;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    [context deleteObject:result[0]];
    [appDelegate saveContext];
    
}

@end
