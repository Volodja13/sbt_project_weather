//
//  UsingCitysServiceProtocol.h
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class City;

@protocol UsingCitysServiceProtocol <NSObject>

- (NSArray*)getSavedCitys;

- (void)saveCity:(City*)city;
- (void)deleteCity:(City*)city;


@end
