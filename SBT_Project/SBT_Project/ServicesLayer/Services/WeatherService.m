//
//  WeatherService.m
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "WeatherService.h"
#import "City.h"
#import "TransportLayer.h"
#import "WeatherMapper.h"

@interface WeatherService()

@property(nonatomic, strong) TransportLayer *transport;

@end

@implementation WeatherService

- (instancetype)initWitTransport:(TransportLayer*)transport{
    self = [super init];
    if(self)
    {
        _transport = transport;
    }
    return self;
}

- (NSURLSessionDataTask *)weatherForCity:(NSArray *)citys
                              completion:(WeatherListCompletionBlock)completion{
   // http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric
    
    NSString *relativePath = @"/data/2.5/group";
    NSMutableDictionary *parametrs = [@{} mutableCopy];
    parametrs[@"APPID"] = @"1d1a7bf78a8881d570fa7091d07b1a76";
    if([citys count] == 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil, [NSArray array]);
        });
        return [NSURLSessionDataTask new];
    }
    NSString *ids = ((City*)citys[0]).geoId.stringValue;
    for (int i = 1; i < [citys count]; ++i) {
        City *city = citys[i];
        ids = [NSString stringWithFormat:@"%@,%@", ids, city.geoId.stringValue];
    }
    parametrs[@"id"] = ids;
    parametrs[@"units"] = @"metric";
    return [self.transport makeGETRequestWithRelativePath:relativePath
                                               parameters:parametrs
                                                  success:^(id response) {
                                                      [self handleSuccsesJson:response completion:completion];
    }
                                                  failure:^(NSError *error) {
                                                      completion(error, nil);
    }];
    
}

- (void)handleSuccsesJson:(NSDictionary*)response completion:(WeatherListCompletionBlock)completion{
    NSMutableArray *res = [WeatherMapper modelFromJson:response];
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(nil, res);
    });
}

@end
