//
//  WeatherServiceProtocol.h
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransportLayer;

typedef void (^WeatherListCompletionBlock)(NSError *error, NSArray *weather);

@protocol WeatherServiceProtocol <NSObject>

- (instancetype)initWitTransport:(TransportLayer*)transport;

- (NSURLSessionDataTask *)weatherForCity:(NSArray *)citys
                              completion:(WeatherListCompletionBlock)completion;


@end
