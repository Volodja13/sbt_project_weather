//
//  ServiceFactory.h
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceFactoryProtocol.h"

@interface ServiceFactory : NSObject <ServiceFactoryProtocol>


- (id<UsingCitysServiceProtocol>)createUsingCityService;
- (id<WeatherServiceProtocol>)createWeatherService;

@end
