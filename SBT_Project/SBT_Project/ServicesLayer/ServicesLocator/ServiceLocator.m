//
//  ServiceLocator.m
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ServiceLocator.h"
#import "ServiceFactoryProtocol.h"

@protocol UsingCitysServiceProtocol;
@protocol WeatherServiceProtocol;

@interface ServiceLocator()

@property (nonatomic, strong) id <ServiceFactoryProtocol> serviceFactory;

@end

@implementation ServiceLocator

#pragma mark - Initialization

static ServiceLocator *sharedInstance = nil;

+ (instancetype)shared {
    return [self sharedService];
}

+ (instancetype)sharedService {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


#pragma mark - Implementation

- (void)setServiceFactory:(id <ServiceFactoryProtocol>)serviceFactory {
    _serviceFactory = serviceFactory;
}

- (id<UsingCitysServiceProtocol>)usingCitysService{
    return [self.serviceFactory createUsingCityService];
}

- (id<WeatherServiceProtocol>)weatherService{
    return [self.serviceFactory createWeatherService];
}

@end
