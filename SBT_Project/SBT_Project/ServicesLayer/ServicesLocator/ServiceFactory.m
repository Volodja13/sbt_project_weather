//
//  ServiceFactory.m
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ServiceFactory.h"
#import "UsingCitysService.h"
#import "WeatherService.h"
#import "TransportLayer.h"

@implementation ServiceFactory



- (id<UsingCitysServiceProtocol>)createUsingCityService{
    return [[UsingCitysService alloc] init];
}


- (id<WeatherServiceProtocol>)createWeatherService{
    return [[WeatherService alloc] initWitTransport:[TransportLayer manager]];
}
@end
