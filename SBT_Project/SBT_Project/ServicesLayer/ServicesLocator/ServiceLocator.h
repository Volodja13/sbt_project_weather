//
//  ServiceLocator.h
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServiceFactoryProtocol;
@protocol UsingCitysServiceProtocol;
@protocol WeatherServiceProtocol;

@interface ServiceLocator : NSObject

@property (nonatomic, strong, readonly) id <UsingCitysServiceProtocol> usingCitysService;
@property (nonatomic, strong, readonly) id <WeatherServiceProtocol> weatherService;

+ (instancetype)shared;
- (void)setServiceFactory:(id <ServiceFactoryProtocol>)serviceFactory;

@end
