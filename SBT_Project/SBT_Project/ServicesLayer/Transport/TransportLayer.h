//
//  TransportLayer.h
//  SBT_Project
//
//  Created by Vovan on 11.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

typedef void (^SuccessCompletionBlock)(id response);
typedef void (^FailureCompletionBlock)(NSError *error);

@interface TransportLayer : AFHTTPSessionManager

+ (instancetype)manager;

- (NSURLSessionDataTask *)makeGETRequestWithRelativePath:(NSString *)relativePath
                                              parameters:(NSDictionary *)parameters
                                                 success:(SuccessCompletionBlock)successBlock
                                                 failure:(FailureCompletionBlock)failure;

@end
