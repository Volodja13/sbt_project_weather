//
//  TransportLayer.m
//  SBT_Project
//
//  Created by Vovan on 11.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "TransportLayer.h"

@implementation TransportLayer

static TransportLayer *sharedInstance = nil;

+ (instancetype)manager {
    return [self sharedManager];
}

+ (instancetype)sharedManager {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] initSharedInstance];
    });
    return sharedInstance;
}

- (instancetype)initSharedInstance {
    NSURL *baseURL = [NSURL URLWithString:@"http://api.openweathermap.org"];
    self = [super initWithBaseURL:baseURL];
    if (self) {
        self.requestSerializer = [AFJSONRequestSerializer new];
        self.responseSerializer = [AFJSONResponseSerializer new];
        
    }
    return self;
}

- (NSURLSessionDataTask *)makeGETRequestWithRelativePath:(NSString *)relativePath
                                              parameters:(NSDictionary *)parameters
                                                 success:(SuccessCompletionBlock)successBlock
                                                 failure:(FailureCompletionBlock)failure {
    return [self GET:relativePath
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 successBlock(responseObject);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure(error);
             }
            ];
}

@end
