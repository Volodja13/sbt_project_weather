//
//  CitysMapper.m
//  SBT_Project
//
//  Created by Vovan on 13.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CitysMapper.h"
#import "City.h"

@implementation CitysMapper


+ (NSMutableArray*)modelFromJson:(NSDictionary*)json{
    NSMutableArray *res = [@[] mutableCopy];
    
    for (NSDictionary *dict in json) {
        City *city = [City new];
        city.geoId = dict[@"id"];
        city.city = dict[@"name"];
        [res addObject:city];
    }
    
    return res;
}

@end
