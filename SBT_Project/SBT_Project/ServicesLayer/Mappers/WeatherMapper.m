//
//  WeatherMapper.m
//  SBT_Project
//
//  Created by Vovan on 13.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "WeatherMapper.h"
#import "CityWeather.h"

@implementation WeatherMapper


+ (NSMutableArray*)modelFromJson:(NSDictionary*)json{
//    CityWeather *w = [CityWeather new];
//    w.city = @"qwertt";
//    w.temp = @(123);
//    return [NSMutableArray arrayWithObject:w];
//
    NSMutableArray *result = [@[] mutableCopy];
    NSDictionary *list = json[@"list"];
    for (NSDictionary *cityDictinary in list) {
        CityWeather *weather = [CityWeather new];
        weather.city = cityDictinary[@"name"];
        weather.geoId = cityDictinary[@"id"];
        NSDictionary *main = cityDictinary[@"main"];
        weather.temp = main[@"temp"];
        weather.pressure = main[@"pressure"];
        weather.humidity = main[@"humidity"];
        NSDictionary *cloud = cityDictinary[@"clouds"];
        weather.cloudness = cloud[@"all"];
        NSDictionary *wind = cityDictinary[@"wind"];
        weather.windSpeed = wind[@"speed"];
        [result addObject:weather];
    }
    return result;
}

@end
