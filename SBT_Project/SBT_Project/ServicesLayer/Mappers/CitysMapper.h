//
//  CitysMapper.h
//  SBT_Project
//
//  Created by Vovan on 13.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CitysMapper : NSObject

+ (NSMutableArray*)modelFromJson:(NSDictionary*)json;

@end
