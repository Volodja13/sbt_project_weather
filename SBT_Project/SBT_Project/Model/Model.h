//
//  Model.h
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelProtocol.h"


@interface Model : NSObject <ModelProtocol>


- (instancetype)init;

- (NSArray *)getUsingCitys;
- (NSArray *)getCitys;
- (void)deleteWeatherWithIndexPath:(NSIndexPath*)indexPath;
- (void)setCitysWithFilter:(NSString*)patern;
- (NSArray *)getWeatherForUsingCitys;
- (void)setCityWithID:(NSNumber*)geoID useValue:(BOOL)value;

- (void)loadingWeatherForUsingCityWithSuccessBlock:(SuccessBlockLoadingData)success
                                       failedBlock:(FailedBlockLoadingData)fail;



- (CityWeather *)getWeatherForCityWithId:(NSNumber*)geoId;

@end
