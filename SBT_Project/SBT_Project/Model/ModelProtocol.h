//
//  ModelProtocol.h
//  SBT_Project
//
//  Created by Vovan on 25.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CityWeather;

typedef void(^SuccessBlockLoadingData)();
typedef void(^FailedBlockLoadingData)(NSString *errorDescription);

@protocol ModelProtocol <NSObject>

@required

- (NSArray *)getUsingCitys;
- (NSArray *)getCitys;
- (void)deleteWeatherWithIndexPath:(NSIndexPath*)indexPath;
- (void)setCitysWithFilter:(NSString*)patern;
- (NSArray *)getWeatherForUsingCitys;
- (void)setCityWithID:(NSNumber*)geoID useValue:(BOOL)value;

- (void)loadingWeatherForUsingCityWithSuccessBlock:(SuccessBlockLoadingData)success
                                       failedBlock:(FailedBlockLoadingData)fail;



- (CityWeather *)getWeatherForCityWithId:(NSNumber*)geoId;

@end
