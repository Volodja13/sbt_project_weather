//
//  CityWeather.h
//  SBT_Project
//
//  Created by Vovan on 24.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityWeather : NSObject


@property(nonatomic, strong) NSNumber *geoId;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, strong) NSNumber *temp;
@property(nonatomic, strong) NSNumber *windSpeed;
@property(nonatomic, copy) NSString *icon;
@property(nonatomic, strong) NSNumber *pressure;
@property(nonatomic, strong) NSNumber *humidity;
@property(nonatomic, strong) NSNumber *cloudness;

@end
