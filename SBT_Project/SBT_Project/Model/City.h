//
//  City.h
//  SBT_Project
//
//  Created by Vovan on 25.05.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject


@property(nonatomic, strong) NSNumber *geoId;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, assign) BOOL use;

@end
