//
//  Model.m
//  SBT_Project
//
//  Created by Vovan on 12.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "Model.h"
#import "ServiceLocator.h"
#import "CityWeather.h"
#import "CitysMapper.h"
#import "City.h"
#import "UsingCitysServiceProtocol.h"
#import "WeatherServiceProtocol.h"
@import UIKit;

@interface Model()

@property(nonatomic, strong) NSMutableDictionary *citys;
@property(nonatomic, strong) NSMutableArray *usingCitys;
@property(nonatomic, strong) NSMutableDictionary *prepareCityDictionary;
@property(nonatomic, strong) NSMutableArray *weather;

@property(nonatomic, strong) NSArray *returnCitys;

@end


@implementation Model

- (instancetype)init{
    self = [super init];
    if(self)
    {
        _citys = [@{} mutableCopy];
        _weather = [@[] mutableCopy];
        NSString *nameCityFile = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"city.json"];
        NSData *jsonData = [NSData dataWithContentsOfFile:nameCityFile];
        NSError *error;
        NSDictionary *jsonDictinary = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                      options:NSJSONReadingMutableContainers
                                                                        error:&error];
        NSArray *jsonArray = [CitysMapper modelFromJson:jsonDictinary];
        for (City *city in jsonArray) {
            _citys[city.geoId.stringValue] = city;
        }
        [self prepareData];
    }
    return self;
}

- (void)deleteWeatherWithIndexPath:(NSIndexPath*)indexPath{
    [self.weather removeObjectAtIndex:indexPath.row];
}

- (void)prepareData{
    _usingCitys = [NSMutableArray arrayWithArray:[[[ServiceLocator shared] usingCitysService] getSavedCitys]];
    _prepareCityDictionary = [NSMutableDictionary dictionaryWithDictionary:self.citys];
    for (City *city in _usingCitys) {
        ((City*)_prepareCityDictionary[city.geoId.stringValue]).use = YES;
    }
    self.returnCitys = [NSArray arrayWithArray:self.prepareCityDictionary.allValues];
}

- (NSArray *)getUsingCitys{
    return self.usingCitys;
}

- (NSArray *)getCitys{
    return self.returnCitys;
}

- (void)setCitysWithFilter:(NSString*)patern{
    NSArray *result;
    if([patern length] > 0)
    {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.city contains[c] %@",
                                        patern];
        result = [self.prepareCityDictionary.allValues filteredArrayUsingPredicate:resultPredicate];
    }
    else
    {
        result = self.prepareCityDictionary.allValues;
    }
    self.returnCitys = result;
}

- (NSArray *)getWeatherForUsingCitys{
    return self.weather;
    
}

- (void)setCityWithID:(NSNumber*)geoID useValue:(BOOL)value{
    if(value)
    {
        [[[ServiceLocator shared] usingCitysService] saveCity:self.citys[geoID.stringValue]];
        [self.usingCitys addObject:self.citys[geoID.stringValue]];
        ((City*)self.prepareCityDictionary[geoID.stringValue]).use = YES;
        for(City *city in self.returnCitys)
        {
            if([city.geoId isEqualToNumber:geoID])
            {
                city.use = YES;
                break;
            }
        }
        //self.returnCitys = [NSArray arrayWithArray:self.prepareCityDictionary.allValues];
    }
    else
    {
        [[[ServiceLocator shared] usingCitysService] deleteCity:self.citys[geoID.stringValue]];
        ((City*)self.prepareCityDictionary[geoID.stringValue]).use = NO;
        //[self.usingCitys removeObject:self.citys[geoID.stringValue]];
        int index = -1;
        for (int i = 0; i < [self.usingCitys count]; ++i) {
            City *city = self.usingCitys[i];
            if([city.geoId isEqualToNumber:geoID])
            {
                index = i;
                break;
            }
        }
        if(index != -1)
        {
            [self.usingCitys removeObjectAtIndex:index];
        }
        for(City *city in self.returnCitys)
        {
            if([city.geoId isEqualToNumber:geoID])
            {
                city.use = NO;
                break;
            }
        }
        //self.returnCitys = [NSArray arrayWithArray:self.prepareCityDictionary.allValues];
    }
    
    
}

- (void)loadingWeatherForUsingCityWithSuccessBlock:(SuccessBlockLoadingData)success
                                       failedBlock:(FailedBlockLoadingData)fail{
    self.weather = [@[] mutableCopy];
    for (City *city in self.usingCitys) {
        CityWeather *weather = [CityWeather new];
        weather.geoId = city.geoId;
        weather.city = city.city;
        weather.icon = @"null";
        [self.weather addObject:weather];
    }
    success();
    [[[ServiceLocator shared] weatherService] weatherForCity:self.usingCitys completion:^(NSError *error, NSArray *weather) {
        self->_weather = [NSMutableArray arrayWithArray:weather];
        success();
    }];
}



- (CityWeather *)getWeatherForCityWithId:(NSNumber*)geoId{
    return [CityWeather new];
}

@end
