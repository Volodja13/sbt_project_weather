//
//  Assembly.m
//  SBT_Project
//
//  Created by Vovan on 13.06.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "Assembly.h"
#import "ServiceLocator.h"
#import "ServiceFactory.h"

@implementation Assembly

+ (void)assemblyServiceLocator{
    ServiceLocator *serviceLocator = [ServiceLocator shared];
    [serviceLocator setServiceFactory:[ServiceFactory new]];
}

@end
